﻿namespace FizzBuzz.Console
{
    using System;
    using System.Linq;

    class Program
    {
        static void Main(string[] args)
        {
            foreach(int iterator in Enumerable.Range(1,100))
            {
                if (iterator % 15 == 0)
                {
                    Console.WriteLine("FizzBuzz");
                }

                else if (iterator % 3 == 0)
                {
                    Console.WriteLine("Fizz");
                }
                else if (iterator % 5 == 0)
                {
                    Console.WriteLine("Buzz");
                }
                else Console.WriteLine(iterator.ToString());
            }
            Console.ReadLine();
        }
    }
}
